//
// RealityCapture s.r.o.
//
// This file 'SfmUtils.cpp' is part of an RC Engine Samples.
//

#include "stdafx.h"
#include "SfmUtils.h"
#include <tchar.h> 
#include <stdio.h>
#include <strsafe.h>
#pragma comment(lib, "User32.lib")

#define ADD_IMAGE(imageName) hr = spSfm->AddImage((imageName), NULL, NULL, 0); if (FAILED(hr)) return hr;


bool IsImageFile( __in_z const wchar_t *pFileName )
{
	#define NUM_ALLOWED_EXTS 5
	wchar_t exts[NUM_ALLOWED_EXTS][5] = { L"jpg", L"jpeg", L"png", L"tif", L"tiff" };
	
	size_t len = wcslen( pFileName );
	size_t i = len - 2;
	while ( ( i > 0 ) && ( pFileName[i] != L'.' ) )
	{
		i--;
	};

	if ( ( i > 0 ) && ( pFileName[i] == L'.' ) )
	{
		const wchar_t *pExt = &pFileName[i+1];
		len = len - i - 1;

		for ( UINT j = 0; j < NUM_ALLOWED_EXTS; j++ )
		{
			bool isOk = true;
			size_t extsLen = wcslen( exts[j] );
			if ( extsLen == len )
			{
				for ( size_t k = 0; k < extsLen; k++ )
				{
					if ( towlower( pExt[k] ) != exts[j][k] )
					{
						isOk = false;
						break;
					}
				};

				if ( isOk )
				{
					return true;
				};
			}
		};
	}

	return false;
};

HRESULT CreateCliExampleSfm(
    __in_z      const wchar_t*											pImagesDirectory,
    __in        CapturingReality::IResourceCache*                       pCache,
    __in        CapturingReality::IConfig*                              pConfig,
    __deref_out CapturingReality::Sfm::IStructureFromMotion**           ppSfm
)
{
    if ( ( pCache == NULL ) || ( pConfig == NULL ) || ( pImagesDirectory == NULL ) )
    {
        return E_INVALIDARG;
    }

    // Load the sensor database if present. It is not mandatory, but
    // prior knowledge about the sensor can speed-up the alignation.

    CComPtr< CapturingReality::Sfm::ISfmSensorDatabase > spSensors;
    CreateSfmSensorDatabase( L"sensorsdb.xml", &spSensors ); // spSensorsDb == NULL on error

    HRESULT hr = S_OK;

    //
    // Create a SFM structure. We register the images,
    // but don't align yet. It is done in CreateReconstruction() in case
    // there is aligned reconstruction found from previous runs.
    //

    CComPtr< CapturingReality::Sfm::IStructureFromMotion > spSfm;
    if ( SUCCEEDED( hr ) )
    {
        hr = CapturingReality::Sfm::CreateSfmPipeline( pConfig, NULL, NULL, pCache, spSensors, NULL, &spSfm );
    }

    if ( SUCCEEDED( hr ) )
    {
        // Add images.
		WIN32_FIND_DATA ffd;
		TCHAR szDir[MAX_PATH];
		size_t length_of_arg;
		HANDLE hFind = INVALID_HANDLE_VALUE;
		DWORD dwError=0;
   
		// Check that the input path plus 3 is not longer than MAX_PATH.
		// Three characters are for the "\*" plus NULL appended below.
		StringCchLength(pImagesDirectory, MAX_PATH, &length_of_arg);

		if (length_of_arg > (MAX_PATH - 3))
		{
			_tprintf( TEXT( "\nDirectory path is too long.\n" ) );
			return (-1);
		}

		_tprintf( TEXT("\nTarget directory is %s\n\n"), pImagesDirectory );

		// Prepare string for use with FindFile functions.  First, copy the
		// string to a buffer, then append '\*' to the directory name.

		StringCchCopy(szDir, MAX_PATH, pImagesDirectory);
		StringCchCat(szDir, MAX_PATH, TEXT("\\*"));

		// Find the first file in the directory.

		hFind = FindFirstFile(szDir, &ffd);

		if (INVALID_HANDLE_VALUE == hFind) 
		{
			_tprintf( TEXT("Error in FindFirstFile\n\n") );
			return E_FAIL;
		} 
   
		// List all the files in the directory with some info about them.
		do
		{
			if ( ( !( ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) ) && ( IsImageFile( ffd.cFileName ) ) )
			{
				TCHAR fileNameWithPath[ 2 * MAX_PATH ];
				StringCchCopy( fileNameWithPath, MAX_PATH, pImagesDirectory );
				StringCchCat( fileNameWithPath, MAX_PATH, TEXT("\\") );
				StringCchCat( fileNameWithPath, MAX_PATH, ffd.cFileName );
								
				ADD_IMAGE( fileNameWithPath );

				_tprintf( TEXT("Adding image :  %s \n"), ffd.cFileName );
			}
		}
		while (FindNextFile(hFind, &ffd) != 0);
    }

    if ( SUCCEEDED( hr ) )
    {
        *ppSfm = spSfm.Detach();
    }

    return hr;
}

HRESULT CreateReconstruction(
    __in        CapturingReality::Sfm::IStructureFromMotion*    pSfm,
    __deref_out CapturingReality::Sfm::ISfmReconstruction**     ppReconstruction
)
{
    if ( pSfm == NULL )
    {
        return E_INVALIDARG;
    }

    HRESULT hr = S_OK;

    //
    // Align SFM structures with registered input images to obtain a reconstruction.
    //

    if ( SUCCEEDED( hr ) )
    {
        printf( "Calculating camera parameters\n" );
        hr = pSfm->RegisterImages( NULL );
    }

    //
    // More components can be reconstructed. We select the largest one,
    // which is, most likely, the most interesting one.
    //

    UINT nReconstructions = 0;

    if ( SUCCEEDED( hr ) )
    {
        hr = UnlockReconstructionsIfNeeded( pSfm );
    }

    if ( SUCCEEDED( hr ) )
    {
        nReconstructions = pSfm->GetReconstructionsCount();
        if ( nReconstructions == 0 )
        {
            *ppReconstruction = NULL;
            return hr;
        }
    }

    UINT largestComponentIndex = 0;

    if ( SUCCEEDED( hr ) )
    {
        UINT largestComponentCameraCount = 0;

        printf( "Calculated %d components\n", nReconstructions );

        for (UINT ireconstruction = 0; ireconstruction < nReconstructions; ireconstruction++)
        {
            CComPtr< CapturingReality::Sfm::ISfmReconstruction > spReconstruction;
            UINT structureSize;

            hr = pSfm->GetReconstruction( ireconstruction, &spReconstruction );
            if ( SUCCEEDED( hr ) )
            {
                hr = spReconstruction->GetStructureSize( &structureSize );
            }
            if ( SUCCEEDED( hr ) )
            {
                UINT cameraCount = spReconstruction->GetCamerasCount();
                printf( "Component %d consists of %d cameras and %d global points\n", ireconstruction, cameraCount, structureSize );

                if ( cameraCount >= largestComponentCameraCount )
                {
                    largestComponentIndex = ireconstruction;
                    largestComponentCameraCount = cameraCount;
                }
            }
            else
            {
                break;
            }
        }
    }

    // Select the component with the largest number of aligned cameras.

    if ( SUCCEEDED( hr ) )
    {
        hr = pSfm->GetReconstruction( largestComponentIndex, ppReconstruction );
    }

    return hr;
}

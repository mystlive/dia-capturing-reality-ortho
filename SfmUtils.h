//
// RealityCapture s.r.o.
//
// This file 'SfmUtils.h' is part of an RC Engine Samples.
//

#pragma once

enum MyResourceType
{
    MRT_COMPONENT = 0xF000,
};

HRESULT CreateCliExampleSfm(
    __in_z      const wchar_t*											imagesDirectory,
    __in        CapturingReality::IResourceCache*                       pCache,
    __in        CapturingReality::IConfig*                              pConfig,
    __deref_out CapturingReality::Sfm::IStructureFromMotion**           ppSfm
);

HRESULT CreateReconstruction(
    __in        CapturingReality::Sfm::IStructureFromMotion*    pSfm,
    __deref_out CapturingReality::Sfm::ISfmReconstruction**     ppReconstruction
);

//
// RealityCapture s.r.o.
//
// This file 'CliExample.cpp' is part of an RC Engine Samples.
//
#include "stdafx.h"
#include "SfmUtils.h"
#include "..\SamplesCommon\TransformationHelpers.h"

using namespace CapturingReality;

const double RAD_TO_DEG = (180 / 3.1415926535897932384626433832795);

const UINT projectionWidth  = 1400; // in pixels
const UINT projectionHeight = 1400; // in pixels
const double projectionDepth = 100.0; // in meters

// res = a + b 
template <class T> inline void Vect3Add( _In_reads_(3) const T *a, _In_reads_(3) const T* b, _Out_writes_(3) T* res)
{
    res[0] = a[0] + b[0];
    res[1] = a[1] + b[1];
    res[2] = a[2] + b[2];
}

void PrintGpsLatitude(__in double src)
{
	double angle, min, sec;
	sec = abs(src) * RAD_TO_DEG;
	angle = floor(sec);
	sec = (sec - angle) * 60;
	min = floor(sec);
	sec = (sec - min) * 60;
//    fprintf(fp, "%c%.0f,%.0f,%.2f", src >= 0 ? 'N' : 'S' , angle, min, sec );
}

void PrintGpsLongitude(__in double src)
{
    double angle, min, sec;
	sec = abs(src) * RAD_TO_DEG;
	angle = floor(sec);
	sec = (sec - angle) * 60;
	min = floor(sec);
	sec = (sec - min) * 60;
//    fprintf(fp, "%c%.0f,%.0f,%.2f", src >= 0 ? 'E' : 'W', angle, min, sec );
}

inline void MulV3M33( const CoordinateSystemPoint &cs, const double *M, __out_ecount(3) double *b)
{
	b[0] = (M[0]*cs.x + M[3]*cs.y + M[6]*cs.z);
	b[1] = (M[1]*cs.x + M[4]*cs.y + M[7]*cs.z);
	b[2] = (M[2]*cs.x + M[5]*cs.y + M[8]*cs.z);
}

//
// This example shows how to process images in a directory to a textured mesh.
//
// It saves registration and model. If you will call it with the same arguments then it will re-use already computed data.
//
//      Scheme:
//          * activate license
//          * create cache
//          * align and save / load component
//          * create and save / load model
//          * filter model
//          * colorize and calculate model texture
//          * export model to obj
//

HRESULT GetParameter( __in int argc, __in _TCHAR* argv[], __in const wchar_t key, __in UINT bufferSize, __out_ecount_z_opt(bufferSize) wchar_t *pValue )
{
	for ( int i = 0; i < argc; i++ )
	{
		if ( ( wcslen(argv[i]) == 2 ) && ( argv[i][0] == L'-' ) && ( argv[i][1] == key ) )
		{
			if ( pValue != NULL )
			{
				if ( i+1 < argc )
				{
					wcscpy_s( pValue, bufferSize, argv[i+1] );
				}
				else
				{
					return E_FAIL;
				}
			}
			return S_OK;
		}
	}
	return E_FAIL;
}

void EnsureCorrectPathFormat( __in UINT bufferSize, __inout_ecount_z_opt(bufferSize) wchar_t *pPath )
{
	size_t len = wcslen( pPath );
	if ( ( pPath[ len - 1 ] != L'\\' ) && ( len+1 < bufferSize ) )
	{
		pPath[ len ] = L'\\';
		pPath[ len+1 ] = 0;
	}
}

HRESULT GetFolderParameter( __in int argc, __in _TCHAR* argv[], __in const wchar_t key, __in UINT bufferSize, __out_ecount_z_opt(bufferSize) wchar_t *pValue )
{
	HRESULT hr = GetParameter( argc, argv, key, bufferSize, pValue );
	if ( SUCCEEDED( hr ) )
	{
		EnsureCorrectPathFormat( bufferSize, pValue );
		if ( !PathFileExists( pValue ) )
		{
			hr = E_FAIL;
		}
	}

	return hr;
}

int _tmain(int argc, _TCHAR* argv[])
{
	#define BUFFER_SIZE 2048
	wchar_t logFileName[BUFFER_SIZE] = L"_rcengine.log";
	wchar_t inputFolder[BUFFER_SIZE] = {0};
	wchar_t outputFolder[BUFFER_SIZE] = {0};
	wchar_t name[BUFFER_SIZE] = {0};


	LogMessageToFile( logFileName, "Running command line : " );
	for ( int i = 0; i < argc; i++ )
	{
		LogMessageToFile( logFileName, "%ls ", argv[i] );
	};
	LogMessageToFile( logFileName, "\n\n" );




	HRESULT hr = GetFolderParameter( argc, argv, L'i', BUFFER_SIZE, inputFolder );
	if ( SUCCEEDED( hr ) )
	{
		hr = GetFolderParameter( argc, argv, L'o', BUFFER_SIZE, outputFolder );
		if ( SUCCEEDED( hr ) )
		{
			hr = GetParameter( argc, argv, L'n', BUFFER_SIZE, name );
			if ( FAILED( hr ) )
			{
				LogMessageToFile( logFileName, "ERROR: The name is not provided.\n\n" );
			}
		}
		else
		{
			LogMessageToFile( logFileName, "ERROR: The output folder is not either defined or does not exist.\n\n" );
		}
	}
	else
	{
		LogMessageToFile( logFileName, "ERROR: The input folder is not either defined or does not exist.\n\n" );
	}
	
	if ( FAILED( hr ) )
	{
		printf( "This is RealityCapture Engine command line sample provided by Capturing Reality company.\n" );
		printf( "\n" );
		printf( "Usage: \n" );
		printf( "\tCliExample.exe -i ImagesFolder -o OutputFolder -n Name [-p TempFolder] [-d preview|normal|high] [-fsct]\n" );
		printf( "\n" );
		printf( "Options:\n" );
		printf( "\t-i ImagesFolder \t Full path to the folder that contains images to be processed.\n" );
		printf( "\t-o OutputFolder \t Full path to the folder where results will be saved.\n" );
		printf( "\t-n Name \t\t The name will be used as a prefix to result names. Must be one word.\n" );
		printf( "\t-p TempFolder \t Optional parameter for full path to the temp folder .\n" );
		printf( "\t-d \t\t\t Model detail. Normal is default.\n" );
		printf( "\t-f \t\t\t Perform filtering.\n" );
		printf( "\t-s \t\t\t Simplify to 10%% of the model triangles count.\n" );
		printf( "\t-c \t\t\t Colorize vertices of the model.\n" );
		printf( "\t-t \t\t\t Texture model.\n" );
		printf( "\t-x \t\t\t Calculate ortho projection and export to KMZ.\n" );
		printf( "\n" );
	}
	else
	{
		LogMessageToFile( logFileName, "Activating RealityCapture ENGINE ...\n\n" );
		hr = ActivateSdk();
	}

    //
    // __ create cache __
    //

    // Creating cache is optional for Sfm creation process but required for Mvs / Multipart meshing.

    CComPtr< CapturingReality::IConfig > spConfig;
    CComPtr< CapturingReality::IResourceCache > spCache;

    if ( SUCCEEDED( hr ) )
    {
		wchar_t tempFolder[2048] = {0};
		if ( SUCCEEDED( GetFolderParameter( argc, argv, L'p', BUFFER_SIZE, tempFolder ) ) )
		{
	        hr = CapturingReality::Utilities::CreateResourceCache( CapturingReality::RCL_CUSTOM, tempFolder, &spCache );
		}
		else
		{
	        hr = CapturingReality::Utilities::CreateResourceCache( CapturingReality::RCL_CUSTOM, L"_crTmp", &spCache );
		}
    }

    if ( SUCCEEDED( hr ) )
    {
        hr = CapturingReality::Utilities::CreateConfig( &spConfig );
    }

    //
    // __ obtain component __
    //

    // Import the one from previous run if present or create a new one.

    CComPtr< CapturingReality::Sfm::IStructureFromMotion > spSfm;
    CComPtr< CapturingReality::Sfm::ISfmReconstruction > spSfmReconstruction;

    if ( SUCCEEDED( hr ) )
    {
		hr = CreateCliExampleSfm( inputFolder, spCache, spConfig, &spSfm );
    }

    if ( SUCCEEDED( hr ) )
    {
		wchar_t componentFileName[BUFFER_SIZE] = {0};
		wcscpy_s( componentFileName, BUFFER_SIZE, outputFolder );
		wcscat_s( componentFileName, BUFFER_SIZE, name );
		wcscat_s( componentFileName, BUFFER_SIZE, L".rcalign" );

        if ( PathFileExists( componentFileName ) == TRUE )
        {
            LogMessageToFile( logFileName, "Importing component ... \n" );
            ImportRCComponent( componentFileName,  spSfm, &spSfmReconstruction );
        }
        else
        {
            LogMessageToFile( logFileName, "Aligning ... \n" );
            hr = CreateReconstruction( spSfm, &spSfmReconstruction );
            if ( SUCCEEDED( hr ) )
            {
                LogMessageToFile( logFileName, "Exporting component ... \n" );
                hr = ExportRCComponent( componentFileName, spSfm, spSfmReconstruction );
            }
        }
    }
	
	//
	// __ create model __
	//

	// Use automatically created reconstruction box.
	GlobalReconstructionVolume region;
	if ( SUCCEEDED( hr ) )
	{
		hr = ApproximateReconstructionBox( &region, spSfm, spSfmReconstruction, false );
	}

	// Create model.
	CComPtr< CapturingReality::Mvs::IMvsModel > spMvsModel;

	if ( SUCCEEDED( hr ) )
	{
		LogMessageToFile( logFileName, "Computing model ... \n" );

		wchar_t detail[2048] = {0};
		if ( SUCCEEDED( GetParameter( argc, argv, L'd', BUFFER_SIZE, detail ) ) )
		{
			if ( detail[0] == L'p' )
			{
				hr = CreateModelPreview( &region, spCache, spConfig, spSfm, spSfmReconstruction, &spMvsModel );
			}
			else if ( detail[0] == L'h' )
			{
				hr = CreateModel( 1, &region, spCache, spConfig, spSfm, spSfmReconstruction, &spMvsModel );
			}
			else
			{
				hr = CreateModel( 2, &region, spCache, spConfig, spSfm, spSfmReconstruction, &spMvsModel );
			}
		}
		else
		{
			hr = CreateModel( 2, &region, spCache, spConfig, spSfm, spSfmReconstruction, &spMvsModel );
		}
	}

	if ( SUCCEEDED( hr ) )
	{
		LogMessageToFile( logFileName, "Unlocking model ... \n" );
		hr = UnlockModelIfNeeded( spSfm, spSfmReconstruction, spMvsModel );
	}

	
    //
    // __ filter model if requested __
    //

	if ( SUCCEEDED( hr ) && ( SUCCEEDED( GetParameter( argc, argv, L'f', BUFFER_SIZE, NULL ) ) ) )
	{
        LogMessageToFile( logFileName, "Filtering model ... \n" );
		CComPtr< CapturingReality::Mvs::IMvsModel > spMvsModelFiltered;
		hr = FilterSample01( 20.0f, spMvsModel, &spMvsModelFiltered );
		if ( SUCCEEDED( hr ) )
		{
			spMvsModel.Release();
			spMvsModel = NULL;
			spMvsModel = spMvsModelFiltered;
		}
	}

    //
    // __ simplify model if requested __
    //

	if ( SUCCEEDED( hr ) && ( SUCCEEDED( GetParameter( argc, argv, L's', BUFFER_SIZE, NULL ) ) ) )
	{
		CComPtr< CapturingReality::Mvs::IMvsModel > spMvsModelSimplified;
        LogMessageToFile( logFileName, "Simplifying model ... \n" );
		hr = SimplifyRelative( 0.1, spMvsModel, &spMvsModelSimplified );
		if ( SUCCEEDED( hr ) )
		{
			spMvsModel.Release();
			spMvsModel = NULL;
			spMvsModel = spMvsModelSimplified;
		}
	}

    //
    // __ colorize model if requested __
    //

	if ( SUCCEEDED( hr ) && ( SUCCEEDED( GetParameter( argc, argv, L'c', BUFFER_SIZE, NULL ) ) ) )
	{
        LogMessageToFile( logFileName, "Calculating model vertex colors ... \n" );

        hr = ColorizeModel( 1, spCache, spSfm, spSfmReconstruction, spMvsModel );
    }

    //
    // __ texture model if requested __
    //

	if ( SUCCEEDED( hr ) && ( SUCCEEDED( GetParameter( argc, argv, L't', BUFFER_SIZE, NULL ) ) ) )
    {
        LogMessageToFile( logFileName, "Texturing model ... \n" );
        hr = TextureModel( 1, spCache, spSfm, spSfmReconstruction, spMvsModel );
    }
	
    if ( SUCCEEDED( hr ) )
    {
        LogMessageToFile( logFileName, "Exporting model to obj ... \n" );
		hr = ExportModelObj( outputFolder, name, spCache, spSfm, spSfmReconstruction, spMvsModel );
    }


    //
    // __ create and export an ortho projection (gps) __
    //

	// Check the flags from CR
	bool is_o_option = SUCCEEDED(GetParameter( argc, argv, L'x', BUFFER_SIZE, NULL ));
	bool is_reconst = spSfmReconstruction->GetReconstructionFlags();
	bool is_gcp =SCCF_GROUND_CONTROL;
	bool is_metric = SCCF_METRIC;

    if ( SUCCEEDED( hr ) && ( SUCCEEDED( GetParameter( argc, argv, L'x', BUFFER_SIZE, NULL ) ) ) && ( spSfmReconstruction->GetReconstructionFlags() && (SCCF_GROUND_CONTROL | SCCF_METRIC) ) )
    {
		CComPtr< CapturingReality::CoordinateSystems::ICoordinateSystem > spGpsCoordSystem;
		const wchar_t* g_gps_name = L"GPS (WGS 84)";
		const wchar_t* g_gps_definition = L"+proj=longlat +datum=WGS84 +no_defs";
		hr = CapturingReality::CoordinateSystems::CreateCoordinateSystem( g_gps_name, g_gps_definition, &spGpsCoordSystem );
        if ( SUCCEEDED( hr ) )
        {
			ISfmCameraModel *pCameraModel = spSfmReconstruction->GetCameraModel();
			CoordinateSystemAnchor anchor = spSfmReconstruction->GetAnchor();
			CoordinateSystemGroundPlane groundPlane = spSfmReconstruction->GetGroundPlane();

			CoordinateSystemPoint centreRegion;
			centreRegion.x = region.position.x;
			centreRegion.y = region.position.y;
			centreRegion.z = region.position.z;

			CoordinateSystemPoint centreGps;

			MulV3M33( centreRegion, groundPlane.R, &centreGps.x );
			Vect3Add( &centreGps.x, &anchor.x, &centreGps.x );

			hr = spGpsCoordSystem->ToModel( 1, &centreGps );
       
			if ( SUCCEEDED( hr ) )
			{
				double longitude = ( centreGps.x / XM_PI ) * 0.5;
				double latitude  = -( log( tan( centreGps.y ) + ( 1.0 / cos( centreGps.y ) ) ) / XM_PI ) * 0.5;
				double altitude  = centreGps.z;

				printf( " Calculated position latitude: " );
				PrintGpsLatitude( latitude );
                    
				printf( " longitude: ");
				PrintGpsLongitude( latitude );

				printf( " altitude:%.2f\n", altitude );
				

				// TODO
				const double north = 63.7220418742507 * DEG_TO_RAD;
				const double south = 63.7204966958123 * DEG_TO_RAD;
				const double east  = 20.0596314452294 * DEG_TO_RAD;
				const double west  = 20.0561460717712 * DEG_TO_RAD;
				const double pixelSizeInRadiansX = ( east  -  west ) / 1024.0;
				const double pixelSizeInRadiansY = ( north - south ) / 1024.0;



				CComPtr< CapturingReality::OrthoProjection::IOrthographicProjection > spOrthoProjection;
				CComPtr< CapturingReality::OrthoProjection::IOrthoProjectionDataProvider > spOrthoProjectionDataProvider;

				if ( SUCCEEDED( hr ) )
				{
					printf( "Calculating orthographic projection... \n" );

					hr = CalculateAxisAlignedOrthoProjection(
						&centreGps, projectionWidth, projectionHeight,
						projectionDepth, pixelSizeInRadiansX, pixelSizeInRadiansY,
						g_gps_name, g_gps_definition,
						spCache, spSfmReconstruction, spMvsModel, &spOrthoProjection
					);
				}

				// Export into kzm.

				if ( SUCCEEDED( hr ) )
				{
					hr = CapturingReality::ModelTools::CreateOrthoPhotoDataProvider( spOrthoProjection, NULL, &spOrthoProjectionDataProvider );
				}

				if ( SUCCEEDED( hr ) )
				{
					bool bGeoTaggsWritten;
					hr = CapturingReality::ModelTools::ExportOrthoProjectionToGeoTiff(
						CapturingReality::OrthoProjection::WorldCoordSystemCentreType::WCSCT_GLOBAL,
						CapturingReality::OrthoProjection::WorldCoordSystemAxesType::WCSAT_GLOBAL_COORDINATE_SYSTEM,
						CapturingReality::OrthoProjection::OrthoPixelType::OPT_COLOR,
						CapturingReality::OrthoProjection::CompressionType::CT_NONE,
						spOrthoProjectionDataProvider,
						L"orthoPhoto.tiff",
						false,
						NULL,
						&bGeoTaggsWritten
					);
				}

				if ( SUCCEEDED( hr ) )
				{
					hr = ExportWorldFile( L"orthoPhoto.tfw", spOrthoProjection );
				}

				if ( SUCCEEDED( hr ) )
				{
					wchar_t kmzFileName[BUFFER_SIZE] = {0};
					wcscpy_s( kmzFileName, BUFFER_SIZE, outputFolder );
					wcscat_s( kmzFileName, BUFFER_SIZE, L"orthoPhoto.kmz" );

					CapturingReality::OrthoProjection::OrthoImageExportParameters parameters;
					parameters.filename = kmzFileName;
					parameters.gcpsCount = 0;
					parameters.pGcps = NULL;
					parameters.imageQuality = 90;
					parameters.kmlExportCompressed = true;
					parameters.kmlTileExtension = L"png";
					parameters.kmlTileSide = 1024;
					parameters.kmlTileWicCodecGUID = GUID_ContainerFormatPng;

					hr = CapturingReality::ModelTools::ExportOrthoPhotoToKml( &parameters, spOrthoProjectionDataProvider, NULL, NULL );
				}
			}
		}
    }
	
    LogMessageToFile( logFileName, "Completed with code %#X\n\n\n", hr );

    return 0;

}

